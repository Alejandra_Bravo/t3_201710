package view;


import java.util.Scanner;

import controller.Controller;
import model.logic.ClaseRandom;

public class view {
	private static void printMenu(){
		System.out.println("1. Verify if a given expression is a well formed formula");
		System.out.println("2. Sort a well formed formula");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}

	private static String readData(){
		System.out.println("Please type the expression, then press enter: ");
		String rta = new Scanner(System.in).next();
		return rta	;
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		for(;;){
			printMenu();

			int option = sc.nextInt();
			switch(option){
			case 1: String a = readData(); System.out.println("--------- \n"+Controller.expresionBienFormada(a)+"\n---------");
			break;

			case 2: System.out.println(Controller.ordenar(readData()));
			break;

			default: System.out.println("--------- \n Invalid option !! \n---------");
			}
		}
	}
}
