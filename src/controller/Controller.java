package controller;

import model.data_structures.Stack;
import model.logic.ClaseRandom;

public class Controller {
	private static ClaseRandom model = new ClaseRandom();

	public static String expresionBienFormada(String exp){
		return model.expresionBienFormada(exp)+"";
	}

	public static String ordenar(String pPila){
		Stack<String> pila = new Stack<String>();
		for (int i = 0; i < pPila.length(); i++) {
			pila.push(pPila.charAt(i)+"");
		}
		Stack<String> rta = model.ordenarPila(pila);
		String rta2 = "";
		while(rta.size()>0){
			rta2+=rta.pop();
		}
		return rta2;
	}

}
