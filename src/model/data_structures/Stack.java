package model.data_structures;


public class Stack<T> implements IStack<T> {

	private Node<T> first;
	private int size;
	
	public Stack(){
		first = null;
		size = 0;
	}
	
	public void push(T item) {
		Node<T> temp = new Node<T>();
		temp.setItem(item);
		if(first == null)
			first = temp;
		else{
			temp.setNextNode(first);
			first = temp;
		}
		size++;
	}

	public T pop() {
		if(first == null)
			return null;
		else{
			Node<T> temp = first;
			first = first.getNextNode();
			size--;
			return temp.getItem();
		}
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public int size() {
		return size;
	}
	
}
