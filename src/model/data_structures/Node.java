package model.data_structures;

public class Node<T> {

	private T item;
	private Node<T> nextNode;
	
	public Node(){
		item = null;
		nextNode = null;
	}
	
	public void setItem(T pItem){
		item = pItem;
	}
	
	public void setNextNode(Node<T> pNode){
		nextNode = pNode;
	}
		
	public T getItem(){
		return item;
	}
	
	public Node<T> getNextNode(){
		return nextNode;
	}
}
