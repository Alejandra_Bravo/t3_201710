package model.data_structures;


public class Queue<T> implements IQueue<T> {
	
	private Node<T> first;
	private int size;
	
	public Queue(){
		first = null;
		size = 0;
	}

	public void enqueue(T item) {
		Node<T> temp = new Node<T>();
		temp.setItem(item);
		if(first == null)
			first = temp;
		else{
			first.setNextNode(temp);
		}
		size++;
	}

	public T dequeue() {
		if(first == null)
			return null;
		else{
			Node<T> temp = first;
			first = first.getNextNode();
			size--;
			return temp.getItem();
		}
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public int size() {
		return size;
	}

}
