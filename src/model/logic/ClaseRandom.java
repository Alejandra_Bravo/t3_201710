package model.logic;

import model.data_structures.Queue;
import model.data_structures.Stack;

public class ClaseRandom {

	public boolean expresionBienFormada(String expresion){
		if(expresion.isEmpty())
			return false;
		else{
			Stack<String> st = new Stack<String>();
			for (int i = 0; i < expresion.length(); i++) {
				if((expresion.charAt(i)+"").equals("[")||(expresion.charAt(i)+"").equals("(")){
					st.push(expresion.charAt(i)+"");
				}
				else if((expresion.charAt(i)+"").equals(")")){
					String s = st.pop();
					if(!s.equals("(")){
						return false;
					}
				}
				else if((expresion.charAt(i)+"").equals("]")){
					String s = st.pop();
					if(!s.equals("[")){
						return false;
					}
				}
			}
		}
		return true;
	}

	public Stack<String> ordenarPila(Stack<String> pPila){
		String str = "",opr = "",num = "";
		Stack<String> rta = new Stack<String>();
		while(pPila.size()>0){
			String s = pPila.pop();
			if(s.equals("[")||s.equals("(")||s.equals(")")||s.equals("]")){
				str+=s;
			}
			else if(s.equals("+")||s.equals("-")||s.equals("*")||s.equals("/")){
				opr+=s;
			}
			else
				num+=s;
		}
		for (int i = 0; i < num.length(); i++) {
			rta.push(num.charAt(i)+"");
		}
		for (int i = 0; i < opr.length(); i++) {
			rta.push(opr.charAt(i)+"");
		}
		for (int i = 0; i < str.length(); i++) {
			rta.push(str.charAt(i)+"");
		}
		return rta;
	}
}
