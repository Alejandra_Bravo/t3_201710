package data_structures.test;

import model.data_structures.Stack;
import junit.framework.TestCase;

public class StackTest extends TestCase{
	private Stack<String> pila;

	public void setupEscenario1(){
		pila = new Stack<String>();
	}
	public void setupEscenario2(){
		pila = new Stack<String>();
		pila.push("elem1");
		pila.push("elem2");
		pila.push("elem3");
		pila.push("elem4");
		pila.push("elem5");
	}
	public void testStack(){
		setupEscenario1();
		assertEquals("El n�mero de elementos deber�a ser cero",0,pila.size());
	}
	public void testPop(){
		//Cuando se inicializa la pila vac�a
		setupEscenario1();
		String rta1 = pila.pop();
		assertNull("El elemento no deber�a existir", rta1);

		//Cuando se inicializa la pila con objetos
		setupEscenario2();
		assertEquals("Deber�a devolver el quinto elemento","elem5",pila.pop());
		assertEquals("Deber�a retornar 4",4,pila.size());
	}
	public void testPush(){
		setupEscenario1();
		pila.push("elem1");
		assertEquals("El n�mero de elementos deber�a ser 1",1,pila.size());
		setupEscenario2();
		pila.push("elem6");
		assertEquals("El n�mero de elementos deber�a ser seis",6,pila.size());
	}
	public void testIsEmpty(){
		setupEscenario1();
		assertTrue("La pila deber�a estar vac�a", pila.isEmpty());
		setupEscenario2();
		assertFalse("La pila no deber�a estar vac�a", pila.isEmpty());
	}
	public void testSize(){
		setupEscenario1();
		assertEquals("Deber�a retornar cero",0,pila.size());
		setupEscenario2();
		assertEquals("Deber�a retornar cinco",5,pila.size());
	}
}
