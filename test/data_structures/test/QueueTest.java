package data_structures.test;

import model.data_structures.Queue;
import junit.framework.TestCase;

public class QueueTest extends TestCase{

	private Queue<String> cola;

	public void setupEscenario1(){
		cola = new Queue<String>();
	}
	public void setupEscenario2(){
		cola = new Queue<String>();
		cola.enqueue("elem1");
		cola.enqueue("elem2");
		cola.enqueue("elem3");
		cola.enqueue("elem4");
		cola.enqueue("elem5");
	}
	public void testQueue(){
		setupEscenario1();
		assertEquals("El n�mero de elementos deber�a ser cero",0,cola.size());
	}
	public void testDequeue(){
		//Cuando se inicializa la cola vac�a
		setupEscenario1();
		String rta1 = cola.dequeue();
		assertNull("El elemento no deber�a existir", rta1);

		//Cuando se inicializa la cola con objetos
		setupEscenario2();
		assertEquals("Deber�a devolver el primer elemento","elem1",cola.dequeue());
		assertEquals("Deber�a retornar 4",4,cola.size());
	}
	public void testEnqueue(){
		setupEscenario1();
		cola.enqueue("elem1");
		assertEquals("El n�mero de elementos deber�a ser 1",1,cola.size());
		setupEscenario2();
		cola.enqueue("elem6");
		assertEquals("El n�mero de elementos deber�a ser seis",6,cola.size());
	}
	public void testIsEmpty(){
		setupEscenario1();
		assertTrue("La cola deber�a estar vac�a", cola.isEmpty());
		setupEscenario2();
		assertFalse("La cola no deber�a estar vac�a", cola.isEmpty());
	}
	public void testSize(){
		setupEscenario1();
		assertEquals("Deber�a retornar cero",0,cola.size());
		setupEscenario2();
		assertEquals("Deber�a retornar cinco",5,cola.size());
	}
}
